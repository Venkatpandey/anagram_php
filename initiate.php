<?php
    /**
     * Created by PhpStorm.
     * User: venpan
     * Date: 27/10/2018
     * Time: 09:33
     */


include_once ('src/Anagram.php');

    //Load words from file
    $inputWords = file('english_58000_lowercase.txt');

    // Get user entered word
    $UserWord = $argv[1];

    // initialise and start
    $anagram = new Anagram();
    $detectedAnagrams = $anagram->detectAnagrams($UserWord, $inputWords);

    print_r($detectedAnagrams);
