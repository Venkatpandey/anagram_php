<?php

    include_once ('/var/www/Anagram_CS/src/Anagram.php');
    use PHPUnit\Framework\TestCase;

    class AnagramTest extends TestCase
    {

        function test_detectAnagrams_matchedLetters()
        {
            //Arrange
            $test_Anagram = new Anagram();
            $input1 = "tiger";
            $input2 = [
                0 => "tiger"
            ];

            //Act
            $result = $test_Anagram->detectAnagrams($input1, $input2);

            //Assert
            $this->assertContains($input1, $result);
        }

        function test_detectAnagrams_mismatchedLetters()
        {
            //Arrange
            $test_Anagram = new Anagram;
            $input1 = "a";
            $input2 = [
                0 => "b"
            ];

            //Act
            $result = $test_Anagram->detectAnagrams($input1, $input2);

            //Assert
            $this->assertEmpty($result);
        }

        function test_detectAnagrams_inverseWords()
        {
            //Arrange
            $test_Anagram = new Anagram;
            $input1 = "on";
            $input2 = [
                0 => "no"
            ];

            //Act
            $result = $test_Anagram->detectAnagrams($input1, $input2);

            //Assert
            $this->assertContains("no", $result);
        }

        function test_detectAnagrams_multipleWords()
        {
            //Arrange
            $test_Anagram = new Anagram;
            $input1 = "on";
            $input2 = [
                0 => "no",
                1 => "en",
                2 => "that",
            ];


            //Act
            $result = $test_Anagram->detectAnagrams($input1, $input2);

            //Assert
            $this->assertContains("no", $result);
        }


    }
