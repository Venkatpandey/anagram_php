<?php
    class Anagram
    {
        /**
         *
         * Simple function to find anagrams in array provided matched from userword
         *
         * @param       $userWord
         * @param array $possibleMatches
         * @return array
         */
        public function detectAnagrams($userWord, array $possibleMatches)
        {
            $detectedAnagrams = [];
            $wordSplit = str_split($userWord);
            $wordLength = strlen(trim($userWord));
            foreach ($possibleMatches as $word) {
                $ArryWordSplit = str_split(rtrim($word));
                $arryWordLength = strlen(trim($word));

                //Check if the lengths are equal
                $diff = array_filter(
                    $wordSplit,
                    function ($val) use (&$ArryWordSplit) {
                        $key = array_search($val, $ArryWordSplit);
                        if ($key === false)
                            return true;
                        unset($ArryWordSplit[$key]);

                        return false;
                    }
                );

                if ($arryWordLength == $wordLength && empty($diff)) {

                    $detectedAnagrams[] = $word;
                }
            }

            return $detectedAnagrams;
        }

        public function __construct()
        {

        }

    }
